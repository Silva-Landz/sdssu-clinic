// delete patient
$(document).ready(function()    {

    $(document).on('click', '#delete_patient', function(e)   {

        var pid = $(this).attr('data-id');
        SwalDelete(pid);
        e.preventDefault();
    });

    function SwalDelete(pid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "patient/delete",
                    method: "POST",
                    data: {"pid":pid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }

}); 

// delete inventory
$(document).ready(function()    {

    $(document).on('click', '#delete_item', function(e)   {

        var iid = $(this).attr('data-id');
        ItemDelete(iid);
        e.preventDefault();
    });

    function ItemDelete(iid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "inventory/delete",
                    method: "POST",
                    data: {"iid":iid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }

}); 

// delete dental
$(document).ready(function()    {

    $(document).on('click', '#delete_dental', function(e)   {

        var drid = $(this).attr('data-id');
        ItemDelete(drid);
        e.preventDefault();
    });

    function ItemDelete(drid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "record/delete_dental",
                    method: "POST",
                    data: {"drid":drid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }

}); 

// delete medical
$(document).ready(function()    {

    $(document).on('click', '#delete_medical', function(e)   {

        var mrid = $(this).attr('data-id');
        ItemDelete(mrid);
        e.preventDefault();
    });

    function ItemDelete(mrid)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "record/delete_medical",
                    method: "POST",
                    data: {"mrid":mrid},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }

}); 

// delete dispense
$(document).ready(function()    {

    $(document).on('click', '#delete_dispense', function(e)   {

        var did = $(this).attr('data-id');
        ItemDelete(did);
        e.preventDefault();
    });

    function ItemDelete(did)   {
        swal({
            title: "Are you sure?",
            text: "You want to delete this data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Confirm",
            closeOnConfirm: false,
        }, function(isConfirm)   {
            if(isConfirm)   {
                $.ajax({
                    url: base_url + "dispense/delete",
                    method: "POST",
                    data: {"did":did},
                    dataType:"json",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            } else  {
                return;
            }
        });
    }

}); 