<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Record_model extends CI_Model    {

    public function get_all_medical()   {

        $this->db->join('scms_patient', 'scms_patient.pid = scms_medical.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id = scms_medical.inv_id');
        $this->db->join('scms_users', 'scms_users.id = scms_medical.user_id');

        $query = $this->db->get('scms_medical');
        
        if($query->num_rows() > 0)    {
        
            return $query->result();
        
        } else  {
        
            return FALSE;
        }
        
    }
        
    public function get_all_dental()    {

        $this->db->join('scms_patient', 'scms_patient.pid = scms_dental.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id = scms_dental.inv_id');
        $this->db->join('scms_users', 'scms_users.id = scms_dental.user_id');
    
        $query = $this->db->get('scms_dental');
        
        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    
    }

    public function get_specific_medical($id)   {

        $this->db->join('scms_patient', 'scms_patient.pid = scms_medical.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id = scms_medical.inv_id');
        $this->db->join('scms_users', 'scms_users.id = scms_medical.user_id');

        $this->db->where('mr_id', $id);

        $query = $this->db->get('scms_medical');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
        
    }

    public function get_specific_dental($id)   {

        $this->db->join('scms_patient', 'scms_patient.pid = scms_dental.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id = scms_dental.inv_id');
        $this->db->join('scms_users', 'scms_users.id = scms_dental.user_id');

        $this->db->where('dr_id', $id);
    
        $query = $this->db->get('scms_dental');
        
        return ($query->num_rows() > 0) ? $query->result() : FALSE;

    }

    public function create_medical()    {

        $data = array(
            'pid'       => $this->input->post('patient', TRUE), 
            'findings'  => $this->input->post('findings', TRUE), 
            'inv_id'    => $this->input->post('medicine', TRUE),
            'user_id'   => $this->input->post('doctor', TRUE) 
        );

        $this->db->insert('scms_medical', $data);

        return TRUE;

    }

    public function create_dental() {

        $data = array(
            'pid'            => $this->input->post('pname', TRUE), 
            'findings'       => $this->input->post('findings', TRUE), 
            'diagnosis_desc' => $this->input->post('diagnosis', TRUE), 
            'inv_id'         => $this->input->post('medicine', TRUE), 
            'user_id'        => $this->input->post('dentist', TRUE)
            // image upload here 
        );

        $this->db->insert('scms_dental', $data);

        return TRUE;
        
    }

    public function update_medical($id)   {

        $data = array(
            'pid'       => $this->input->post('patient', TRUE), 
            'findings'  => $this->input->post('findings', TRUE), 
            'inv_id'    => $this->input->post('medicine', TRUE),
            'user_id'   => $this->input->post('doctor', TRUE) 
        );

        $this->db->where('mr_id', $id);
        $this->db->update('scms_medical', $data);

        return TRUE;

    }

    public function update_dental($id) {

        $data = array(
            'pid'            => $this->input->post('pname', TRUE), 
            'findings'       => $this->input->post('findings', TRUE), 
            'diagnosis_desc' => $this->input->post('diagnosis', TRUE), 
            'inv_id'         => $this->input->post('medicine', TRUE), 
            'user_id'        => $this->input->post('dentist', TRUE)
            // image upload here 
        );

        $this->db->where('dr_id', $id);
        $this->db->update('scms_dental', $data);

        return TRUE;

    }

}
