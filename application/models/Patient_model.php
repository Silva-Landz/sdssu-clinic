<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_model extends CI_Model    {

    public function __construct()   {

        parent::__construct();

    }

    public function get_all()   {

        $this->db->join('scms_course', 'scms_patient.course_id = scms_course.course_id');
        $this->db->order_by('scms_patient.lname', 'ASC');

        $query = $this->db->get('scms_patient');

        if($query->num_rows() > 0)    {

            return $query->result();

        } else  {

            return FALSE;

        }

    }

    public function register()  {

        $data = array(
            'spid'          => $this->input->post('spid', TRUE),
            'lname'         => $this->input->post('lastname', TRUE),
            'fname'         => $this->input->post('firstname', TRUE),
            'mname'         => $this->input->post('middlename', TRUE),
            'dob'           => $this->input->post('dob', TRUE),
            'address'       => $this->input->post('address', TRUE),
            'course_id'     => $this->input->post('course', TRUE),
            'year'          => $this->input->post('year', TRUE),
            'height'        => $this->input->post('height', TRUE),
            'weight'        => $this->input->post('weight', TRUE),
            'bloodtype'     => $this->input->post('bloodtype', TRUE),
            'tin_no'        => $this->input->post('tin', TRUE),
            'philhealth_no' => $this->input->post('philhealth', TRUE),
            'sLname'        => $this->input->post('slname', TRUE),
            'sFname'        => $this->input->post('sfname', TRUE),
            'sMname'        => $this->input->post('smname', TRUE),
            'sOccupation'   => $this->input->post('soccupation', TRUE),
            'sContact_no'   => $this->input->post('scontact', TRUE),
            'fLname'        => $this->input->post('flname', TRUE),
            'fFname'        => $this->input->post('ffname', TRUE),
            'fMname'        => $this->input->post('fmname', TRUE),
            'fOccupation'   => $this->input->post('foccupation', TRUE),
            'fContact'      => $this->input->post('fcontact', TRUE),
            'mLname'        => $this->input->post('mlname', TRUE),
            'mFname'        => $this->input->post('mfname', TRUE),
            'mMname'        => $this->input->post('mmname', TRUE),
            'mOccupation'   => $this->input->post('moccupation', TRUE),
            'mContact'      => $this->input->post('mocntact', TRUE),
            'patient_type' => $this->input->post('ptype', TRUE),
            // 'spid' => $this->input->post('spid', TRUE),
            // image here
        );

        $this->db->insert('scms_patient', $data);

        if($this->db->affected_rows() > 0)    {

            return TRUE;

        } else  {

            return FALSE;
            
        }

    }

    public function get_patient($id)   {

        $this->db->join('scms_course', 'scms_patient.course_id = scms_course.course_id');

        $query = $this->db->get_where('scms_patient', array('pid' => $id));

        if($query->num_rows() == 1)    {

            return $query->result();

        } else  {

            return FALSE;

        }

    }

    public function update($id)    {

        $data = array(
            'spid'          => $this->input->post('spid', TRUE),
            'lname'         => $this->input->post('lastname', TRUE),
            'fname'         => $this->input->post('firstname', TRUE),
            'mname'         => $this->input->post('middlename', TRUE),
            'dob'           => $this->input->post('dob', TRUE),
            'address'       => $this->input->post('address', TRUE),
            'course_id'     => $this->input->post('course', TRUE),
            'year'          => $this->input->post('year', TRUE),
            'height'        => $this->input->post('height', TRUE),
            'weight'        => $this->input->post('weight', TRUE),
            'bloodtype'     => $this->input->post('bloodtype', TRUE),
            'tin_no'        => $this->input->post('tin', TRUE),
            'philhealth_no' => $this->input->post('philhealth', TRUE),
            'sLname'        => $this->input->post('slname', TRUE),
            'sFname'        => $this->input->post('sfname', TRUE),
            'sMname'        => $this->input->post('smname', TRUE),
            'sOccupation'   => $this->input->post('soccupation', TRUE),
            'sContact_no'   => $this->input->post('scontact', TRUE),
            'fLname'        => $this->input->post('flname', TRUE),
            'fFname'        => $this->input->post('ffname', TRUE),
            'fMname'        => $this->input->post('fmname', TRUE),
            'fOccupation'   => $this->input->post('foccupation', TRUE),
            'fContact'      => $this->input->post('fcontact', TRUE),
            'mLname'        => $this->input->post('mlname', TRUE),
            'mFname'        => $this->input->post('mfname', TRUE),
            'mMname'        => $this->input->post('mmname', TRUE),
            'mOccupation'   => $this->input->post('moccupation', TRUE),
            'mContact'      => $this->input->post('mocntact', TRUE),
            'patient_type' => $this->input->post('ptype', TRUE),
            // 'spid' => $this->input->post('spid', TRUE),
            // image here
        );

        $this->db->where('pid', $id);
        $this->db->update('scms_patient', $data);

        if($this->db->affected_rows() > 0)  {

            return TRUE;

        } else  {

            return FALSE;

        }
        
    }
    
}
