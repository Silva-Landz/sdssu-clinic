<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dispense_model extends CI_Model    {
    
    public function __construct()   {

        parent::__construct();

    }

    public function get_all()   {

        $this->db->join('scms_patient', 'scms_patient.pid=scms_dispense.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id=scms_dispense.inv_id');
        
        $query = $this->db->get('scms_dispense');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;

    }

    public function get_specific($id)  {

        $this->db->join('scms_patient', 'scms_patient.pid=scms_dispense.pid');
        $this->db->join('scms_inventory', 'scms_inventory.inv_id=scms_dispense.inv_id');
        $this->db->where('disp_id', $id);

        $query = $this->db->get('scms_dispense');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;

    }

    public function create()    {
        
        $data = array(
            'pid'      => $this->input->post('patient', TRUE), 
            'illness'  => $this->input->post('illness', TRUE), 
            'inv_id'   => $this->input->post('medicine', TRUE), 
            'quantity' => $this->input->post('quantity', TRUE),
            'date'     => date('Y-m-d') 
        );

        $this->db->insert('scms_dispense', $data);

        // update the stock quantity
        if($this->db->affected_rows() > 0)    {

            $quantity = intval($this->input->post("quantity"));
            $inv_id = $this->input->post('medicine');

            $this->db->set('inv_stocks', "inv_stocks-$quantity", FALSE);
            $this->db->where('inv_id', $inv_id);
            $this->db->update('scms_inventory');
        }

        return TRUE;

    }

    public function update($id)    {

        $data = array(
            'pid'      => $this->input->post('patient', TRUE), 
            'illness'  => $this->input->post('illness', TRUE), 
            'inv_id'   => $this->input->post('medicine', TRUE), 
            'quantity' => $this->input->post('quantity', TRUE),
        );

        $this->db->where('disp_id', $id);
        $this->db->update('scms_dispense', $data);

        return TRUE;
    }

}
