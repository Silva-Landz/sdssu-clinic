<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model  {
    
    public function register_account()  {

        // hash password using sha512
        $password = $this->input->post('password');
        $key = $this->config->item('encryption_key');
        $salt1 = hash('sha512', $key . $password);
        $salt2 = hash('sha512', $password . $key);
        $hashed_password = hash('sha512', $salt1 . $password . $salt2); 

        $data = array(
            'firstname' => $this->input->post('firstname', true),
            'lastname'  => $this->input->post('lastname', true),
            'username'  => $this->input->post('username', true),
            'password'  => $hashed_password,
            'user_type' => $this->input->post('usertype', true),
            'photo'     => $this->input->post('image', true) // waya pa ma implement
        );
        
        $this->db->insert('scms_users', $data);

        return true;
    }

    public function login_account() {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $usertype = $this->input->post('usertype');

        // hash password using sha512
        $key = $this->config->item('encryption_key');
        $salt1 = hash('sha512', $key . $password);
        $salt2 = hash('sha512', $password . $key);
        $hashed_password = hash('sha512', $salt1 . $password . $salt2); 

        $query = $this->db->where('username', $username)
                 ->where('password', $hashed_password)
                 ->where('user_type', $usertype)
                 ->get('scms_users');

        if($query->num_rows() == 1)  {

            return $query->result();

        } else {

            return false;

        }
    }

    public function view_profile($id)  {

        $query = $this->db->get_where('scms_users', array('id' => $id));

        if($query->num_rows() > 0)    {

            return $query->result_array();

        } else {

            return FALSE;

        }

    }

    // public function insert_logs($action)   {

    //     $this->db->insert('scms_logs', $data);

    // }

}
