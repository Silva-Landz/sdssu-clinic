<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model  {
    
    public function __construct()   {

        parent::__construct();

    }

    public function get_all()   {

        $this->db->order_by('inv_name', 'ASC');

        $query = $this->db->get('scms_inventory');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;

    }

    public function get_specific($id)  {

        $this->db->where('inv_id', $id);

        $query = $this->db->get('scms_inventory');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;

    }

    public function add_item()  {

        $data = array(
            'inv_name'          => $this->input->post('description'),
            'inv_category'      => $this->input->post('category'),
            'inv_stocks'        => $this->input->post('quantity'),
            'inv_price'         => $this->input->post('price'),
            'inv_expiration'    => $this->input->post('expiration')
        );

        $this->db->insert('scms_inventory', $data);

        return TRUE;

    }

    public function update_item($id)   {

        $data = array(
            'inv_name'          => $this->input->post('description'),
            'inv_category'      => $this->input->post('category'),
            'inv_stocks'        => $this->input->post('quantity'),
            'inv_price'         => $this->input->post('price'),
            'inv_expiration'    => $this->input->post('expiration')
        );

        $this->db->where('inv_id', $id);
        $this->db->update('scms_inventory', $data);

        return TRUE;

    }

}
