<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()	{

		parent::__construct();

		$this->load->model('admin_model');
		$this->load->model('patient_model');
		$this->load->model('inventory_model');
		$this->load->model('record_model');
		$this->load->model('dispense_model');

	}

	public function templates($path, $data = NULL)	{

		if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'registration' || $this->uri->segment(2) == 'login')	{

			$this->load->view('includes/header', $data);
			$this->load->view($path, $data);
			$this->load->view('includes/footer', $data);

		} else	{

			$data['title'] = 'Forbidden';

			$this->load->view('includes/header', $data);
			$this->load->view('errors/error_403');
			$this->load->view('includes/footer', $data);

		}		
	}
	
	public function login()	{

		$data['title'] = 'Login';

		$this->form_validation->set_rules('username', 'username', 'trim|required|callback_validate');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]',
									array('required' => 'You must provide a %s'));

		if($this->form_validation->run() == FALSE)	{
			
			$this->templates('accounts/login', $data);
		
		} else {

			$data = array(
				'users'		 => $this->admin_model->login_account(),
				'isLoggedIn' => 1
			);

			foreach($data['users'] as $user)	{

				$usertype = $user->user_type;
				$action = "Logged in";

				$log_data = array(
					'user_type' => $usertype,
					'action'	=> $action
				);
				
				$this->db->insert('scms_log', $log_data);
				
			}

			$this->session->set_userdata($data);

			redirect('admin/dashboard');

		}
	}

	public function validate()	{

		if($this->admin_model->login_account())	{

			return TRUE;

		} else {

			$this->form_validation->set_message('validate', 'Incorrect username/password/usertype.');

			return FALSE;

		}
	}

	public function registration()	{

		$data['title'] = 'Registration';

		$this->form_validation->set_rules('firstname', 'firstname', 'required');
		$this->form_validation->set_rules('lastname', 'lastname', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[5]',
									array('required' => 'You must provide a %s'));
		$this->form_validation->set_rules('confirmpass', 'password confirmation', 'required|min_length[5]|matches[password]');
		$this->form_validation->set_rules('usertype', 'user type', 'required');

		if($this->form_validation->run() == FALSE)	{
			
			$this->templates('accounts/register', $data);
		
		} else {

			// if(!empty($this->input->post('image')))	{

			// 	$config = array(
			// 		'upload_path'	=> base_url('uploads/users'),
			// 		'allowed_types' => 'jpg|png|jpeg|svg|gif',
			// 	);

			// 	$this->load->library('upload', $config);

			// 	if(!$this->upload->do_upload('image')) {
			// 		$this->session->set_flashdata(
			// 			array(
			// 				'error' => $this->upload->display_errors()
			// 			));
			// 			redirect('admin/registration');
			// 	} 

			// }

			$this->admin_model->register_account();
			$this->session->set_flashdata('message', '<p class="alert alert-success">Successfully created an account. You can now <a href="../admin/login">login</a></p>');

			redirect('admin/registration');

		}

	}

	public function dashboard()	{

		$data['title'] = 'Dashboard';

		$this->templates('main/dashboard', $data);

	}

	public function patient()	{

		$data['title'] = 'Patient';
		$data['patients'] = $this->patient_model->get_all();

		$this->templates('main/patient', $data);

	}

	public function inventory()	{

		$data['title'] = 'Inventory';
		$data['items'] = $this->inventory_model->get_all();

		$this->templates('main/inventory', $data);

	}

	public function dispense()	{

		$data['title'] = 'Dispense';
		$data['patients'] = $this->db->get('scms_patient')->result();
		$data['medicines'] = $this->db->where('inv_category', 'medicine')->get('scms_inventory')->result();
		$data['dispenses'] = $this->dispense_model->get_all();

		$this->form_validation->set_rules('patient', 'patient&#39s name', 'required');
		$this->form_validation->set_rules('illness', 'illness description', 'required');
		$this->form_validation->set_rules('medicine', 'medicine', 'required');
		$this->form_validation->set_rules('quantity', 'quantity', 'required');

		if($this->form_validation->run() == FALSE)	{

			$this->templates('main/dispense', $data);

		} else	{

			if($this->dispense_model->create())	{

				$this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully created a new dispense.</p>');

			} else	{

				$this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-check-circle"></i> Failed to created a new dispense.</p>');

			}

			redirect('admin/dispense');

		}

	}

	public function records()	{

		$data['title'] = 'Records';
		// fetching all the medical records
		$data['medicals'] = $this->record_model->get_all_medical();
		// fetching all the dental records
		$data['dentals'] = $this->record_model->get_all_dental();

		$this->templates('main/records', $data);

	}

	public function logs()	{

		$data['title'] = 'Logs';
		// fetching all the data logs
		$data['logs'] = $this->db->get('scms_log')->result();

		$this->templates('main/logs', $data);

	}
	
	public function about()	{

		$data['title'] = 'About';

		$this->templates('main/about', $data);

	}

	public function profile($id = NULL)	{

		$data['title'] = "Profile";

		if(!is_null($id) && isset($id))	{

			$data['users'] = $this->admin_model->view_profile($id);

			$this->templates('accounts/profile', $data);

		} else {

			show_404();

		}

	}

	public function logout()	{

		$this->session->sess_destroy();

		redirect('admin/login', 'refresh');

	}
	
}
