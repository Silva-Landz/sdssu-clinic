<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller   {

    public function __construct()   {

        parent::__construct();

        $this->load->model('inventory_model');

    }

    public function templates($path, $data = NULL)	{

		if($this->session->userdata('isLoggedIn') == 1)	{

			$this->load->view('includes/header', $data);
			$this->load->view($path, $data);
			$this->load->view('includes/footer');

		} else	{

			show_404();

		}	

    }
    
    public function create() {

        $categories = $this->input->post('category');

        $data['title'] = 'Add Inventory';

        if($categories == 'medicine' && !empty($categories))    {

            $this->form_validation->set_rules('description', 'description', 'required');
            $this->form_validation->set_rules('category', 'categories', 'required');
            $this->form_validation->set_rules('quantity', 'quantity', 'required');
            $this->form_validation->set_rules('price', 'price of an item', 'required');
            $this->form_validation->set_rules('expiration', 'expiration date', 'required');

        } else  {

            $this->form_validation->set_rules('description', 'description', 'required');
            $this->form_validation->set_rules('category', 'categories', 'required');
            $this->form_validation->set_rules('quantity', 'quantity', 'required');
            $this->form_validation->set_rules('price', 'price of an item', 'required');

        }

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_inventory', $data);

        } else  {

            if($this->inventory_model->add_item())    {

                $this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully created a new item.</p>');

            } else {

                $this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Failed to create an item.</p>');

            }

            redirect('admin/inventory');

        }

    }

    public function update($id)    {

        if(!empty($id) && is_numeric($id) && isset($id))    {

            $categories = $this->input->post('category');

            $data['title'] = 'Add Inventory';
            $data['items'] = $this->inventory_model->get_specific($id);

            if($categories == 'medicine' && !empty($categories))    {

                $this->form_validation->set_rules('description', 'description', 'required');
                $this->form_validation->set_rules('category', 'categories', 'required');
                $this->form_validation->set_rules('quantity', 'quantity', 'required');
                $this->form_validation->set_rules('price', 'price of an item', 'required');
                $this->form_validation->set_rules('expiration', 'expiration date', 'required');

            } else  {

                $this->form_validation->set_rules('description', 'description', 'required');
                $this->form_validation->set_rules('category', 'categories', 'required');
                $this->form_validation->set_rules('quantity', 'quantity', 'required');
                $this->form_validation->set_rules('price', 'price of an item', 'required');

            }

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/update_inventory', $data);

            } else  {

                if($this->inventory_model->update_item($id))    {

                    $this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully updated an item.</p>');

                } else {

                    $this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Failed to update an item.</p>');

                }

                redirect('admin/inventory');

            }

        } else  {

            show_404();

        }

    }

    public function delete()    {

        $iid = intval($this->input->post('iid', TRUE));

        $this->db->where('inv_id', $iid);
        $this->db->delete('scms_inventory');
        
        if($this->db->affected_rows() > 0)    {

            $response['status'] = 'success';
            $response['message'] = 'Patient deleted successfully';

        } else  {

            $response['status'] = 'error';
            $response['message'] = 'Unable to delete patient';
        }

        echo json_encode($response);
    }

}