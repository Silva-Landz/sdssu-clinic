<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dispense extends CI_Controller    {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('dispense_model');

    }

    public function templates($path, $data = NULL)	{

		if($this->session->userdata('isLoggedIn') == 1 || $this->uri->segment(2) == 'registration' || $this->uri->segment(2) == 'login')	{

			$this->load->view('includes/header', $data);
			$this->load->view($path, $data);
			$this->load->view('includes/footer', $data);

		} else	{

			$data['title'] = 'Forbidden';

			$this->load->view('includes/header', $data);
			$this->load->view('errors/error_403');
			$this->load->view('includes/footer', $data);

		}		
	}
    
    public function update($id = NULL)    {

        if(!empty($id) && is_numeric($id) && isset($id))    {
            
            $data['title'] = "Edit dispense";
            $data['patients'] = $this->db->get('scms_patient')->result();
            $data['medicines'] = $this->db->where('inv_category', 'medicine')->get('scms_inventory')->result();
            $data['dispenses'] = $this->dispense_model->get_specific($id);

            $this->form_validation->set_rules('patient', 'patient&#39s name', 'required');
            $this->form_validation->set_rules('illness', 'illness description', 'required');
            $this->form_validation->set_rules('medicine', 'medicine', 'required');
            $this->form_validation->set_rules('quantity', 'quantity', 'required');

            if($this->form_validation->run() == FALSE)	{

                $this->templates('actions/update_dispense', $data);

            } else	{

                if($this->dispense_model->update($id))	{

                    $this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully updated the dispense.</p>');

                } else	{

                    $this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-check-circle"></i> Failed to updated the dispense.</p>');

                }

                redirect('admin/dispense');

            }

        } else  {
            show_404();
        }

    }
	
	public function delete()    {

        $did = intval($this->input->post('did', TRUE));

        $this->db->where('disp_id', $did);
        $this->db->delete('scms_dispense');

        if($this->db->affected_rows() > 0)    {

            $response['status'] = 'success';
			$response['message'] = 'Dispense deleted successfully';

        } else  {

            $response['status'] = 'error';
            $response['message'] = 'Unable to delete dispense';
        }

        echo json_encode($response);
    }

}
