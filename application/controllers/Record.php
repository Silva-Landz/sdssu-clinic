<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Record extends CI_Controller  {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('patient_model');
        $this->load->model('inventory_model');
        $this->load->model('record_model');
        
    }

    public function templates($path, $data = NULL)	{

		if($this->session->userdata('isLoggedIn') == 1)	{

			$this->load->view('includes/header', $data);
			$this->load->view($path, $data);
			$this->load->view('includes/footer');

		} else	{

			show_404();

		}	

    }

    public function create_medical()    {

        $data['title'] = 'Create New Medical Record';
        // fetch all patients information
        $data['patients'] = $this->patient_model->get_all();
        // fetch all inventory informations
        $data['items'] = $this->inventory_model->get_all();
        // fetch all users
        $data['users'] = $this->db->get('scms_users')->result();

        $this->form_validation->set_rules('patient', 'patient&#39s name', 'required');
        $this->form_validation->set_rules('findings', 'findings', 'required');
        $this->form_validation->set_rules('medicine', 'medicine', 'required');
        // $this->form_validation->set_rules('quantity', 'quantity', 'required');
        $this->form_validation->set_rules('doctor', 'doctor', 'required');

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_medical', $data);

        } else  {

            if($this->record_model->create_medical())    {

                $message = '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully created a new medical record.</p>';

            } else  {

                $message = '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to create a new medical record.</p>';

            }

            $this->session->set_flashdata('message', $message);

            redirect('admin/records');
        }

    }
    
    public function create_dental()    {

        $data['title'] = 'Create New Dental Record';
        // fetch all patients information
        $data['patients'] = $this->patient_model->get_all();
        // fetch all inventory informations
        $data['items'] = $this->inventory_model->get_all();
        // fetch all users
        $data['users'] = $this->db->get('scms_users')->result();

        $this->form_validation->set_rules('pname', 'patient&#39s name', 'required');
        $this->form_validation->set_rules('findings', 'findings', 'required');
        $this->form_validation->set_rules('diagnosis', 'diagnosis', 'required');
        $this->form_validation->set_rules('medicine', 'medicine', 'required');
        $this->form_validation->set_rules('dentist', 'dentist', 'required');

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_dental', $data);

        } else  {

            if($this->record_model->create_dental())    {

                $message = '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully created a new dental record.</p>';

            } else  {

                $message = '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to create a new dental record.</p>';

            }

            $this->session->set_flashdata('message', $message);

            redirect('admin/records');

        }

    }
    
    public function update_medical($id)    {

        if(!empty($id) && is_numeric($id) && isset($id))    {

            $data['title'] = "Update medical record";

            $data['patients'] = $this->patient_model->get_all();
            // fetch all inventory informations
            $data['items'] = $this->inventory_model->get_all();
            // fetch all users
            $data['users'] = $this->db->get('scms_users')->result();
            $data['medicals'] = $this->record_model->get_specific_medical($id);

            $this->form_validation->set_rules('patient', 'patient&#39s name', 'required');
            $this->form_validation->set_rules('findings', 'findings', 'required');
            $this->form_validation->set_rules('medicine', 'medicine', 'required');
            $this->form_validation->set_rules('doctor', 'doctor', 'required');

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/update_medical', $data);
    
            } else  {
    
                if($this->record_model->update_medical($id))    {
    
                    $message = '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully updated medical record.</p>';
    
                } else  {
    
                    $message = '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to update medical record.</p>';
    
                }
    
                $this->session->set_flashdata('message', $message);
    
                redirect('admin/records');
            }

        } else  {

            show_404();

        }

    }

    public function update_dental($id)    {

        if(!empty($id) && is_numeric($id) && isset($id))    {

            $data['title'] = 'Update Dental Record';
            // fetch all patients information
            $data['patients'] = $this->patient_model->get_all();
            // fetch all inventory informations
            $data['items'] = $this->inventory_model->get_all();
            // fetch all users
            $data['users'] = $this->db->get('scms_users')->result();
            $data['dentals'] = $this->record_model->get_specific_dental($id);

            $this->form_validation->set_rules('pname', 'patient&#39s name', 'required');
            $this->form_validation->set_rules('findings', 'findings', 'required');
            $this->form_validation->set_rules('diagnosis', 'diagnosis', 'required');
            $this->form_validation->set_rules('medicine', 'medicine', 'required');
            $this->form_validation->set_rules('dentist', 'dentist', 'required');

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/update_dental', $data);

            } else  {

                if($this->record_model->update_dental($id))    {

                    $message = '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully updated dental record.</p>';

                } else  {

                    $message = '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to update dental record.</p>';

                }

                $this->session->set_flashdata('message', $message);

                redirect('admin/records');

            }

        } else  {

            show_404();

        }

    }

    public function delete_medical()    {

        $mrid = intval($this->input->post('mrid', TRUE));

        $this->db->where('mr_id', $mrid);
        $this->db->delete('scms_medical');
        
        if($this->db->affected_rows() > 0)    {

            $response['status'] = 'success';
            $response['message'] = 'Medical record deleted successfully';

        } else  {

            $response['status'] = 'error';
            $response['message'] = 'Unable to delete record';
        }

        echo json_encode($response);
    }

    public function delete_dental()    {

        $drid = intval($this->input->post('drid', TRUE));

        $this->db->where('dr_id', $drid);
        $this->db->delete('scms_dental');
        
        if($this->db->affected_rows() > 0)    {

            $response['status'] = 'success';
            $response['message'] = 'Dental record deleted successfully';

        } else  {

            $response['status'] = 'error';
            $response['message'] = 'Unable to delete record';
        }

        echo json_encode($response);
    }

}
