<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller    {
    
    public function __construct()   {

        parent::__construct();

        $this->load->model('patient_model');

    }

    public function templates($path, $data = NULL)	{

		if($this->session->userdata('isLoggedIn') == 1)	{

			$this->load->view('includes/header', $data);
			$this->load->view($path, $data);
			$this->load->view('includes/footer');

		} else	{

			show_404();

		}	

	}

    public function create()    {

        $data['title'] = 'Add Patient';
        $data['courses'] = $this->db->get('scms_course')->result();

        if($this->input->post('ptype') == 'student')    {
            $this->form_validation->set_rules('course', 'course descripton', 'required');
            $this->form_validation->set_rules('year', 'year level', 'required');
        }

        $this->form_validation->set_rules('spid', 'student id', 'required|is_unique[scms_patient.spid]', array(
            'is_unique' => 'The student id is already existing.'
        ));
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('middlename', 'middlename', 'required');
        $this->form_validation->set_rules('dob', 'date of birth', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');

        if($this->form_validation->run() == FALSE)    {

            $this->templates('actions/add_patient', $data);

        } else  {

            if($this->patient_model->register())    {

                $this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully registered a patient</p>');
                
            } else {

                $this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to register a patient.</p>');

            }

            redirect('admin/patient');

        }

    }

    // get the specific patient and its details.
    public function view($id = NULL)  {

        if($id != NULL)    {

            $data['title'] = 'View Patient Details';
            $data['patients'] = $this->patient_model->get_patient($id);

            $this->templates('actions/read_patient', $data);

        } else {

            show_404();

        }

    }
    
    public function update($id = NULL)  {

        $data['title'] = 'View Patient Details';
        $data['patients'] = $this->patient_model->get_patient($id);

        $this->form_validation->set_rules('spid', 'student id', 'required');
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('middlename', 'middlename', 'required');
        $this->form_validation->set_rules('dob', 'date of birth', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');

        if($id != NULL)    {

            if($this->form_validation->run() == FALSE)    {

                $this->templates('actions/update_patient', $data);

            } else  {

                if($this->patient_model->update($id) == TRUE)    {

                    $this->session->set_flashdata('message', '<p class="alert alert-success"><i class="fa fa-check-circle"></i> Successfully updated a patient information</p>');
                
                } else {

                    $this->session->set_flashdata('message', '<p class="alert alert-danger"><i class="fa fa-exclamation"></i> Failed to update a patient.</p>');

                }

                redirect('admin/patient');
                
            }
 
        } else {

            show_404();

        }

    }

    public function delete()    {

        $pid = intval($this->input->post('pid', TRUE));

        $this->db->where('pid', $pid);
        $this->db->delete('scms_patient');

        if($this->db->affected_rows() > 0)    {

            $response['status'] = 'success';
            $response['message'] = 'Patient deleted successfully';

        } else  {

            $response['status'] = 'error';
            $response['message'] = 'Unable to delete patient';
        }

        echo json_encode($response);
    }

}
    