<?php if(is_object($dentals) || is_array($dentals)):?>
    <?php foreach($dentals as $dental):?>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card border-secondary">
                        <div class="card-header"><b>Update dental record</b></div>
                        <div class="card-body">
                            <form action="<?= base_url('record/update_dental/'). $dental->dr_id; ?>" method="POST">
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="pname">Patient's name</label>
                                        <select name="pname" class="form-control">
                                            <option value="">Choose . . .</option>
                                            <?php if(is_object($patients) || is_array($patients)):?>
                                                <?php foreach($patients as $patient): ?>
                                                    <?php if($dental->pid == $patient->pid): ?>
                                                        <option value="<?= $dental->pid; ?>" selected="selected"><?= $dental->lname.", ".$dental->fname." ".$dental->mname; ?></option>
                                                    <?php else:?>
                                                        <option value="<?= $patient->pid; ?>"><?= $patient->lname.", ".$patient->fname." ".$patient->mname; ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <small class="text text-danger"><?= form_error('pname'); ?></small>
                                    </div>
                                    <div class="form-group col-md-7">
                                        <label for="findings">Findings</label>
                                        <input type="text" class="form-control" name="findings" value="<?= $dental->findings; ?>" placeholder="Enter dentist's findings">
                                        <small class="text text-danger"><?= form_error('findings'); ?></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">  
                                        <label for="diagnosis">Diagnosis description</label>
                                        <textarea name="diagnosis" class="form-control" rows="3"><?= $dental->diagnosis_desc; ?></textarea>
                                        <small class="text text-danger"><?= form_error('diagnosis'); ?></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="medicine">Prescribed medicine</label>
                                        <select name="medicine" class="form-control">
                                            <option value="">Choose . . .</option>
                                            <?php if(is_object($items) || is_array($items)):?>
                                                <?php foreach($items as $item): ?>
                                                <?php
                                                        $exp_date = $item->inv_expiration;
                                                        $tod_date = date('Y-m-d');

                                                        // convert to strtotime
                                                        $exp_date = strtotime($exp_date);
                                                        $tod_date = strtotime($tod_date);
                                                    ?>
                                                    <?php if($item->inv_stocks > 0 && empty($exp_date) || is_null($exp_date)): ?>
                                                        <?php if($dental->inv_id == $item->inv_id):?>
                                                            <option value="<?= $dental->inv_id; ?>" selected="selected"><?= $dental->inv_name; ?></option>
                                                        <?php else: ?>
                                                            <option value="<?= $item->inv_id; ?>"><?= $item->inv_name; ?></option>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                    <?php if($item->inv_stocks > 0 && $tod_date < $exp_date): ?>
                                                        <?php if($dental->inv_id == $item->inv_id):?>
                                                            <option value="<?= $dental->inv_id; ?>" selected="selected"><?= $dental->inv_name; ?></option>
                                                        <?php else: ?>
                                                            <option value="<?= $item->inv_id; ?>"><?= $item->inv_name; ?></option>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <small class="text text-danger"><?= form_error('medicine'); ?></small>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="dentist">Dentist</label>
                                        <select name="dentist" class="form-control">
                                            <option value="">Choose . . .</option>
                                            <?php if(is_object($users) || is_array($users)):?>
                                                <?php foreach($users as $user): ?>
                                                    <?php if($dental->user_id == $user->id):?>
                                                        <option value="<?= $dental->user_id; ?>" selected="selected"><?= $dental->firstname." ". $dental->lastname; ?></option>
                                                    <?php else: ?>
                                                        <option value="<?= $user->id; ?>"><?= $user->firstname." ". $user->lastname; ?></option>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <small class="text text-danger"><?= form_error('dentist'); ?></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-7">  
                                        <label for="upload">Upload dental chart</label>
                                        <input class="form-control" type="file" name="dentalphoto">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary pull-right" name="submit">Update</button>
                                <a href="<?php echo base_url('admin/records'); ?>" class="btn btn-light pull-right mr-2">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php endforeach;?>
<?php endif;?>