
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header"><b>Add Inventory</b></div>
                <form action="<?php echo base_url('inventory/create'); ?>" method="POST">
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" value="<?= set_value('description'); ?>" placeholder="Enter item's description">
                            <small class="text-danger"><?= form_error('description'); ?></small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="category">Category</label>
                            <select name="category" class="form-control">
                                <option value="">Choose . . .</option>
                                <option value="medicine" <?php if(set_value('category') == 'medicine') echo "selected='selected'"?>>Medicine</option>
                                <option value="ppe" <?php if(set_value('category') == 'ppe') echo "selected='selected'"?>>PPE</option>
                                <option value="accessory" <?php if(set_value('category') == 'accessory') echo "selected='selected'"?>>Accessory</option>
                            </select>
                            <small class="text-danger"><?= form_error('category'); ?></small>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" name="quantity" value="<?= set_value('quantity'); ?>" placeholder="No. of items">
                            <small class="text-danger"><?= form_error('quantity'); ?></small>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="price">Price</label>
                            <input type="number" class="form-control" name="price" value="<?= set_value('price'); ?>" placeholder="Enter item's price">
                            <small class="text-danger"><?= form_error('price'); ?></small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="expiration">Expiration date</label>
                            <input type="date" class="form-control" name="expiration" value="<?= set_value('expiration'); ?>" placeholder="Enter item's price">
                            <small class="text-danger"><?= form_error('expiration'); ?></small>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">Register item</button>
                        <a href="<?php echo base_url('admin/inventory'); ?>" class="btn btn-light pull-right mr-2 mb-2">Cancel</a>
                    </div>
                </div>     
            </div>
            </form> 
        </div>
    </div>
</div>