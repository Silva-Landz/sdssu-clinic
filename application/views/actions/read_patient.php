<?php foreach($patients as $patient): ?>
    <a href="<?= base_url('patient/update/'). $patient->pid; ?>" class="btn btn-info btn-sm">Edit</a>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>1</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>PERSONAL DATA</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Course/Department:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>ID No.:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Contact #:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <p>&nbsp;SDSSU-Cantilan-FM-HS-001-0001</p>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Details of Additional Training/Courses Undergone:</p>
                            <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>Institution:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Details of Training:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Period:</p>
                            <p>1.&shy;&shy;&shy;&shy;_________________________&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;__&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________&nbsp;&nbsp;&nbsp;&nbsp; _________________</p>
                            <p>2.&shy;&shy;&shy;&shy;_________________________&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;__&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________&nbsp;&nbsp;&nbsp;&nbsp; _________________</p>
                            <p>3._________________________&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;__&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________&nbsp;&nbsp;&nbsp;&nbsp; _________________</p>
                            <p>4._________________________&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;__&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________&nbsp;&nbsp;&nbsp;&nbsp; _________________</p>
                            <p>5._____________________&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;&shy;______&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________&nbsp;&nbsp;&nbsp;&nbsp; _________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Educational Attainment: </strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>______________________</p>
                            <p>______</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>____________________________&shy;&shy;___</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>_______________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>_______________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Primary:</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Secondary:</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Tertiary:</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Name of School:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>From - to:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Address of School:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Family Backgound: </strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>4.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>3.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>2.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>1.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Occupation</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Name</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Children (List all children whether living with you or not)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Spouse&rsquo;s Name: &nbsp;&nbsp;&nbsp;&nbsp; ______________________________________________________________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;(Last Name),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (First Name)&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Middle Name)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>(If Married)</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>4.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>3.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>2.</p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>1.</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Occupation</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Name</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Sibling(s):</strong></p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Parent&rsquo;s Address: ________________________________________________________________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Street, Purok, Barangay, Municipality, Province)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>If deceased, year and cause of death: _____________________</p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>If living, health status: ______________________</p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Mother&rsquo;s Maiden Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;(Last Name),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (First Name)&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Middle Name)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>If living, health status: ______________________</p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>If deceased, year and cause of death: _____________________&nbsp;</p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Father&rsquo;s Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________________________________________________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(Last Name),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (First Name)&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Middle Name)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p><strong>Background:</strong></p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>&nbsp;</p>
                            <p>Photo</p>
                            <p>2 x 2</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>_________________________</p>
                            <p>Position/Profession</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Philhealth#: _________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SSS&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;#: _________________________</p>
                            <p>Pag-ibig&nbsp;&nbsp;&nbsp; #: _________________________</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Driver&rsquo;s License #:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Professional License #:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Religion:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Weight in kg:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Height:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Date of Birth: ___________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(mm/dd/yr)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Marital Status: ( ) Single&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;( ) Married</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;( ) Widowed</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Age:</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Address: ________________________________________________________________________</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Street, Purok, Barangay, Municipality, Province)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Gender: ( ) Male</p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( ) Female</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <p>Name: _________________________________________ /__________</p>
                            <p>&nbsp;(Last Name,)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (First Name)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Middle Name)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Nickname)</p>
                        </td>
                    </tr>
                </tbody>
                </table>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>

<?php endforeach; ?>