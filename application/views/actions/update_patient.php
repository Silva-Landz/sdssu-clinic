<?php foreach($patients as $patient): ?>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Add Patient's Information</b> (Required)
                </div>
                <div class="card-body">
                    <form action="<?= base_url('patient/update/'). $patient->pid; ?>" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="spid">Patient's I.D <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="spid" value="<?= $patient->spid; ?>" placeholder="Ex. 000-0000">
                                <small class="text-danger"><?= form_error('spid'); ?></small>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="ptype">Type of patient</label>
                                <select name="ptype" class="form-control">
                                    <option value="student">Student</option>
                                    <option value="personnel">Personnel</option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="lastname">Lastname <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="lastname" value="<?= $patient->lname; ?>" placeholder="Enter patient's lastname">
                                <small class="text-danger"><?= form_error('lastname'); ?></small>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="firstname">Firstname <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="firstname" value="<?= $patient->fname; ?>" placeholder="Enter patient's firstname">
                                <small class="text-danger"><?= form_error('firstname'); ?></small>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="middlename">Middlename <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="middlename" value="<?= $patient->mname; ?>" placeholder="Enter patient's middlename">
                                <small class="text-danger"><?= form_error('middlename'); ?></small>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="dob">Date of Birth <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="dob" value="<?= $patient->dob; ?>">
                                <small class="text-danger"><?= form_error('dob'); ?></small>
                            </div>
                            <div class="form-group col-md-9">
                                <label for="address">Address <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="address" value="<?= $patient->address; ?>" placeholder="Enter patient's address">
                                <small class="text-danger"><?= form_error('address'); ?></small>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="course">Course</label>
                                <select name="course" class="form-control">
                                    <option value="0">Choose course . . .</option>
                                    <option value="<?= $patient->course_id; ?>" selected="selected"><?= $patient->course_description; ?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="year">Year</label>
                                <select name="year" class="form-control">
                                    <option value="">Choose . . .</option>
                                    <option value="1" <?php if($patient->year == 1) echo "selected='selected'"?>>1</option>
                                    <option value="2" <?php if($patient->year == 2) echo "selected='selected'"?>>2</option>
                                    <option value="3" <?php if($patient->year == 3) echo "selected='selected'"?>>3</option>
                                    <option value="4" <?php if($patient->year == 4) echo "selected='selected'"?>>4</option>
                                    <option value="5" <?php if($patient->year == 5) echo "selected='selected'"?>>5</option>
                                    <option value="6" <?php if($patient->year == 6) echo "selected='selected'"?>>6</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="height">Height (cm)</label>
                                <input type="number" class="form-control" name="height" placeholder="0">
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="weight">Weight (kg)</label>
                                <input type="number" class="form-control" name="weight" placeholder="0">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="bloodtype">Bloodtype</label>
                                <select name="bloodtype" class="form-control">
                                    <option value="">Choose . . .</option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="tin">TIN no.</label>
                                <input type="text" class="form-control" name="tin" placeholder="Enter TIN No.">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="philhealth">PhilHealth No.</label>
                                <input type="text" class="form-control" name="philhealth" placeholder="Enter Philihealth No.">
                            </div>
                        </div>
                        <hr>
                        <label><b>Spouse Infomation</b></label>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="slname">Lastname</label>
                                <input type="text" class="form-control" name="slname" placeholder="Enter spouse's lastname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="sfname">Firstname</label>
                                <input type="text" class="form-control" name="sfname" placeholder="Enter spouse's firstname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="smname">Middlename</label>
                                <input type="text" class="form-control" name="smname" placeholder="Enter spouse's middlename">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="soccupation">Occupation</label>
                                <input type="text" class="form-control" name="soccupation" placeholder="Enter spouse's occupation">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="scontact">Contact No.</label>
                                <input type="number" class="form-control" name="scontact" placeholder="Ex. 0912345789">
                            </div>
                        </div>
                        <hr>
                        <label><b>Father's Infomation</b></label>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="flname">Lastname</label>
                                <input type="text" class="form-control" name="flname" placeholder="Enter father's lastname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ffname">Firstname</label>
                                <input type="text" class="form-control" name="ffname" placeholder="Enter father's firstname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="fmname">Middlename</label>
                                <input type="text" class="form-control" name="fmname" placeholder="Enter father's middlename">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="foccupation">Occupation</label>
                                <input type="text" class="form-control" name="foccupation" placeholder="Enter father's occupation">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="fcontact">Contact No.</label>
                                <input type="number" class="form-control" name="fcontact" placeholder="Ex. 0912345789">
                            </div>
                        </div>
                        <hr>
                        <label><b>Mother's Infomation</b></label>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="mlname">Lastname</label>
                                <input type="text" class="form-control" name="mlname" placeholder="Enter mother's lastname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mfname">Firstname</label>
                                <input type="text" class="form-control" name="mfname" placeholder="Enter mother's firstname">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mmname">Middlename</label>
                                <input type="text" class="form-control" name="mmname" placeholder="Enter mother's middlename">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="moccupation">Occupation</label>
                                <input type="text" class="form-control" name="moccupation" placeholder="Enter mother's occupation">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mcontact">Contact No.</label>
                                <input type="number" class="form-control" name="mcontact" placeholder="Ex. 0912345789">
                            </div>
                        </div>

                        <hr>
                        <label><b>Choose patient's photo to upload (Optional)</b></label><br>
                        <input type="file" class="form-control" accept="">
                    
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary pull-right">Register patient</button>
                    <a href="<?php echo base_url('admin/patient'); ?>" class="btn btn-light pull-right mr-2"> Cancel</a>
                </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>