<?php if(is_object($dispenses) || is_array($dispenses)):?>
    <?php foreach($dispenses as $dispense):?>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-12">
                    <?= $this->session->flashdata('message'); ?>
                    <div class="card border-secondary">
                        <div class="card-header">
                            <b>Update Dispense</b>
                        </div>
                        <div class="card-body">
                            <form action="<?= base_url('dispense/update/'). $dispense->disp_id; ?>" method="POST">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="pname">Patient's name</label>
                                        <select name="patient" class="form-control">
                                            <option value="">Choose . . .</option>
                                            <?php if(is_object($patients) || is_array($patients)):?>
                                                <?php foreach($patients as $patient):?>
                                                    <?php if($dispense->pid == $patient->pid): ?>
                                                        <option value="<?= $dispense->pid; ?>" selected="selected"><?= $dispense->lname.", ".$dispense->fname." ".$dispense->mname; ?></option>
                                                    <?php else:?>
                                                        <option value="<?= $patient->pid; ?>"><?= $patient->lname.", ".$patient->fname." ".$patient->mname; ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <small class="text text-danger"><?= form_error('patient'); ?></small>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="pname">Illness description</label>
                                        <input type="text" class="form-control" name="illness" value="<?= $dispense->illness; ?>" placeholder="Enter description">
                                        <small class="text text-danger"><?= form_error('illness'); ?></small>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="pname">Prescribed medicine</label>
                                        <select name="medicine" class="form-control">
                                            <option value="">Choose . . .</option>
                                            <?php if(is_object($medicines) || is_array($medicines)):?>
                                                <?php foreach($medicines as $medicine):?>
                                                    <?php
                                                        $exp_date = $item->inv_expiration;
                                                        $tod_date = date('Y-m-d');

                                                        // convert to strtotime
                                                        $exp_date = strtotime($exp_date);
                                                        $tod_date = strtotime($tod_date);
                                                    ?>
                                                    <?php if($medicine->inv_stocks > 0 && empty($exp_date) || is_null($exp_date)): ?>
                                                            <?php if($dispense->inv_id == $medicine->inv_id):?>
                                                                <option value="<?= $dispense->inv_id; ?>" selected="selected"><?= $dispense->inv_name; ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $medicine->inv_id; ?>"><?= $medicine->inv_name; ?></option>
                                                            <?php endif;?>
                                                    <?php endif;?>  
                                                    <?php if($medicine->inv_stocks > 0 && $tod_date < $exp_date): ?>
                                                            <?php if($dispense->inv_id == $medicine->inv_id):?>
                                                                <option value="<?= $dispense->inv_id; ?>" selected="selected"><?= $dispense->inv_name; ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $medicine->inv_id; ?>"><?= $medicine->inv_name; ?></option>
                                                            <?php endif;?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <small class="text text-danger"><?= form_error('medicine'); ?></small>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label for="pname">Quantity</label>
                                        <input type="number" class="form-control" name="quantity" value="<?= $dispense->quantity; ?>" placeholder="0">
                                        <small class="text text-danger"><?= form_error('quantity'); ?></small>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>