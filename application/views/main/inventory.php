<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <div class="card border-secondary">
                <div class="card-header"><b>Inventory</b></div>
                <div class="card-body">
                    <a href="<?php echo base_url('inventory/create'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add item</a>
                    <table id="patients" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ITEM ID</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Stocks</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Expiration</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($items) || is_object($items)): ?>
                            <?php foreach($items as $item): ?>
                            <tr>
                                <td><?= $item->inv_id; ?></td>
                                <td><?= $item->inv_name; ?></td>
                                <td><?= ucfirst($item->inv_category); ?></td>
                                <td>
                                    <?php 
                                        if( $item->inv_stocks <= 0)    {
                                            echo '<small class="badge badge-danger">Out of Stock</small>';
                                        } else  {
                                            echo $item->inv_stocks;
                                        }
                                    ?>
                                </td>
                                <td><?= "P". $item->inv_price; ?></td>
                                <td>
                                    <!-- Check medicine expiration -->
                                    <?php
                                        $exp_date = $item->inv_expiration;
                                        $tod_date = date('Y-m-d');

                                        // convert to strtotime
                                        $exp_date = new DateTime($exp_date);
                                        $tod_date = new DateTime($tod_date);
                                        $interval = date_diff($exp_date, $tod_date);

                                        // echo $days_before_exp->format('%d days');
                                        if($item->inv_category == 'medicine')    {
                                            if($tod_date >= $exp_date)    {
                                                echo "<p class='badge badge-danger'>Expired</p>";
                                            } elseif($interval->format('%d days') <= 20)
                                                echo "<p class='badge badge-warning'>". $interval->format('%d days') ." left</p>";
                                            else  {
                                                echo "<p class='badge badge-success'>Good</p>";
                                            }
                                        }
                                    ?>
                                </td>
                                <td>
                                    <!-- Convert date to string -->
                                    <?php 
                                        $str_date = date("M jS, Y",strtotime($item->inv_expiration));
                                        // print the converted date
                                        echo $str_date;
                                    ?>
                                </td>
                                <td><a href="<?= base_url('inventory/update/'). $item->inv_id; ?>" class="text-secondary"><i class="fa fa-pencil"></i></a></td>
                                <td>
                                    <a href="javascript:void(0)" class="text-secondary" id="delete_item" data-id="<?= $item->inv_id; ?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>