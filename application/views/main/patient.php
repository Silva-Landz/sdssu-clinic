<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <div class="card border-secondary">
                <div class="card-header"><b>Patient's Information</b></div>
                <div class="card-body">
                    
                <?php 
        
                    $users = $this->session->userdata('users');
                    // Start of the loop 
                    foreach($users as $user): 
                        
                ?>

                    <?php if($user->user_type == 'administrator'): ?>
                        <a href="<?php echo base_url('patient/create'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add patient</a>
                    <?php endif; ?>
                    
                    <table id="patients" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>PID</th>
                                <th>ID</th>
                                <th>Fullname</th>
                                <th>Patient type</th>
                                <th>Course and Year</th>
                                <th>Address</th>
                                <th></th>
                                <?php if($user->user_type == 'administrator'):?>
                                <th></th>
                                <?php endif;?>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_array($patients) || is_object($patients)): ?>
                                <?php foreach($patients as $patient): ?>
                                    <tr>
                                        <td><?= $patient->pid; ?></td>
                                        <td><?= $patient->spid; ?></td>
                                        <td><?= $patient->lname .", ". $patient->fname ." ". substr($patient->mname, 0, 1) ."."; ?></td>
                                        <td><?= ucfirst($patient->patient_type); ?></td>
                                        <td><?= $patient->course_code ." - ". $patient->year; ?></td>
                                        <td><?= $patient->address; ?></td>
                                        <td><a href="<?= base_url('patient/view/'). $patient->pid; ?>" class="text-secondary" title="View"><i class="fa fa-eye"></i></a></td>
                                        <?php if($user->user_type == 'administrator'):?>
                                        <td>
                                            <a href="javascript:void(0)" class="text-secondary" id="delete_patient" data-id="<?= $patient->pid; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                                        </td>
                                        <?php endif;?>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                
                <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>