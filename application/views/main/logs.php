<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">History logs</div>
                <div class="card-body">
                    <table id="patients" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Usertype</th>
                                <th>Action</th>
                                <th>Time and Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($logs) || is_object($logs)): ?>
                            <?php foreach($logs as $log): ?>
                                <tr>
                                    <td><?= $log->log_id; ?></td>
                                    <td><?= $log->user_type; ?></td>
                                    <td><?= $log->action; ?></td>
                                    <td><?= $log->logged_time; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>