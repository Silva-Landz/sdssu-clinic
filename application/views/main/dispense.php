<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Add Dispense</b>
                </div>
                <div class="card-body">
                    <form action="<?= base_url('admin/dispense'); ?>" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="pname">Patient's name</label>
                                <select name="patient" class="form-control">
                                    <option value="">Choose . . .</option>
                                    <?php if(is_object($patients) || is_array($patients)):?>
                                        <?php foreach($patients as $patient):?>
                                            <option value="<?= $patient->pid; ?>"><?= $patient->lname.", ".$patient->fname." ".$patient->mname; ?></option>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                                <small class="text text-danger"><?= form_error('patient'); ?></small>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="pname">Illness description</label>
                                <input type="text" class="form-control" name="illness" placeholder="Enter description">
                                <small class="text text-danger"><?= form_error('illness'); ?></small>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="pname">Prescribed medicine</label>
                                <select name="medicine" class="form-control">
                                    <option value="">Choose . . .</option>
                                    <?php if(is_object($medicines) || is_array($medicines)):?>
                                        <?php foreach($medicines as $medicine):?>
                                            <?php
                                                $exp_date = $item->inv_expiration;
                                                $tod_date = date('Y-m-d');

                                                // convert to strtotime
                                                $exp_date = strtotime($exp_date);
                                                $tod_date = strtotime($tod_date);
                                            ?>
                                            <?php if($medicine->inv_stocks > 0 && empty($exp_date) || is_null($exp_date)): ?>
                                                <option value="<?= $medicine->inv_id; ?>"><?= $medicine->inv_name; ?></option>
                                            <?php endif;?>  
                                            <?php if($medicine->inv_stocks > 0 && $tod_date < $exp_date): ?>
                                                <option value="<?= $medicine->inv_id; ?>"><?= $medicine->inv_name; ?></option> 
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </select>
                                <small class="text text-danger"><?= form_error('medicine'); ?></small>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="pname">Quantity</label>
                                <input type="number" class="form-control" name="quantity" placeholder="0">
                                <small class="text text-danger"><?= form_error('quantity'); ?></small>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Dispense List</b>
                    <a href="#" class="text-dark pull-right"><i class="fa fa-print"></i> Print</a></div>
                <div class="card-body">
                    <table id="dispense" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Patient's name</th>
                                <th>Illness</th>
                                <th>Prescribed Medicine</th>
                                <th>Quantity</th>
                                <th>Date given</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($dispenses) || is_array($dispenses)):?>
                                <?php foreach($dispenses as $dispense): ?>
                                <tr>
                                    <td><?= $dispense->disp_id; ?></td>
                                    <td><?= $dispense->lname.", ". $dispense->fname." ".$dispense->mname; ?></td>
                                    <td><?= $dispense->illness; ?></td>
                                    <td><?= $dispense->inv_name; ?></td>
                                    <td><?= $dispense->quantity; ?></td>
                                    <td><?= $dispense->date; ?></td>
                                    <td><a href="<?= base_url('dispense/update/'). $dispense->disp_id; ?>" class="text-secondary" title="Update"><i class="fa fa-pencil"></i></a></td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-secondary" id="delete_dispense" data-id="<?= $dispense->disp_id; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>