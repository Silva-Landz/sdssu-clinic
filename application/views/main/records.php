<!-- Medical Information -->
<div class="container">
    
    <?php 
    
    $users = $this->session->userdata('users');

    foreach($users as $user): 
            
    ?>
    
    <?php if($user->user_type == 'administrator'): ?>

    <div class="row mt-5">
        <div class="col-md-12">
            <?= $this->session->flashdata('message'); ?>
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Medical Information</b>
                    <a href="#" class="text-dark pull-right"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url('record/create_medical'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add medical</a>
                    <table id="patients" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Patient's Name</th>
                                <th>Findings</th>
                                <th>Prescribed Medicine</th>
                                <th>Doctor</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($medicals) || is_array($medicals)):?>
                                <?php foreach($medicals as $medical): ?>
                                <tr>
                                    <td><?= $medical->mr_id; ?></td>
                                    <td><?= $medical->lname.", ". $medical->fname." ".$medical->mname; ?></td>
                                    <td><?= $medical->findings; ?></td>
                                    <td><?= $medical->inv_name; ?></td>
                                    <td><?= $medical->firstname." ". $medical->lastname; ?></td>
                                    <td><a href="<?= base_url('record/update_medical/'). $medical->mr_id; ?>" class="text-secondary" title="Update"><i class="fa fa-pencil"></i></a></td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-secondary" id="delete_medical" data-id="<?= $medical->mr_id; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Dental Information -->
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Dental Information</b>
                    <a href="#" class="text-dark pull-right"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url('record/create_dental'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add dental</a>
                    <table id="dental" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Patient's Name</th>
                                <th>Diagnosis</th>
                                <th>Findings</th>
                                <th>Medicine</th>
                                <th>Dentist</th>
                                <th>Dental Chart</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($dentals) || is_array($dentals)):?>
                                <?php foreach($dentals as $dental): ?>
                                <tr>
                                    <td><?= $dental->dr_id; ?></td>
                                    <td><?= $dental->lname.", ". $dental->fname." ".$dental->mname; ?></td>
                                    <td><?= $dental->diagnosis_desc; ?></td>
                                    <td><?= $dental->findings; ?></td>
                                    <td><?= $dental->inv_name; ?></td>
                                    <td><?= $dental->firstname ." ". $dental->lastname; ?></td>
                                    <td></td>
                                    <td><a href="<?= base_url('record/update_dental/'). $dental->dr_id; ?>" class="text-secondary" title="Update"><i class="fa fa-pencil"></i></a></td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-secondary" id="delete_dental" data-id="<?= $dental->dr_id; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <?php if($user->user_type == 'doctor'): ?>

    <!-- Medical Information -->
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Medical Information</b>
                    <a href="#" class="text-dark pull-right"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url('record/create_medical'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add medical</a>
                    <table id="patients" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Patient's Name</th>
                                <th>Findings</th>
                                <th>Prescribed Medicine</th>
                                <th>Doctor</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($medicals) || is_array($medicals)):?>
                                <?php foreach($medicals as $medical): ?>
                                <tr>
                                    <td><?= $medical->mr_id; ?></td>
                                    <td><?= $medical->lname.", ". $medical->fname." ".$medical->mname; ?></td>
                                    <td><?= $medical->findings; ?></td>
                                    <td><?= $medical->inv_name; ?></td>
                                    <td><?= $medical->firstname." ". $medical->lastname; ?></td>
                                    <td><a href="#" class="text-secondary" title="Update"><i class="fa fa-pencil"></i></a></td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-secondary" id="delete_medical" data-id="<?= $medical->mr_id; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <?php if($user->user_type == 'dentist'): ?>
    
    <!-- Dental Information -->
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header">
                    <b>Dental Information</b>
                    <a href="#" class="text-dark pull-right"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="card-body">
                    <a href="<?php echo base_url('record/create_dental'); ?>" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Add dental</a>
                    <table id="dental" class="table table-striped table-hover">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Patient's Name</th>
                                <th>Diagnosis</th>
                                <th>Findings</th>
                                <th>Medicine</th>
                                <th>Dentist</th>
                                <th>Image dental</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(is_object($dentals) || is_array($dentals)):?>
                                <?php foreach($dentals as $dental): ?>
                                <tr>
                                    <td><?= $dental->dr_id; ?></td>
                                    <td><?= $dental->lname.", ". $dental->fname." ".$dental->mname; ?></td>
                                    <td><?= $dental->diagnosis_desc; ?></td>
                                    <td><?= $dental->findings; ?></td>
                                    <td><?= $dental->inv_name; ?></td>
                                    <td><?= $dental->firstname ." ". $dental->lastname; ?></td>
                                    <td></td>
                                    <td><a href="#" class="text-secondary" title="Update"><i class="fa fa-pencil"></i></a></td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-secondary" id="delete_dental" data-id="<?= $dental->dr_id; ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <?php endforeach; ?>

</div>