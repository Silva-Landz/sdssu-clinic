<?php foreach($users as $user): ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card mt-5">
                <!-- <div class="card-header"><b>Profile</b></div> -->
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-4 text-center">
                            <img src="<?php echo base_url('uploads/users/default.png'); ?>" alt="default-image" width="180" height="180">
                        </div>
                        <div class="col-md-">
                            <h1 class="display-4 pt-4"><?php echo $user['firstname'] ." ". $user['lastname']; ?></h1>
                            <p style="font-size:1.2rem"><b><?php echo ucfirst($user['user_type']); ?></b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card border-secondary mt-5">
                <div class="card-header"><b>Change account password</b></div>
                <form>
                    <div class="card-body">
                    
                        <div class="form-group row">
                            <label class="col-sm-4 text-right">Old password: </label>
                            <input type="password" class="form-control col-sm-6" name="oldpass" placeholder="Password">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 text-right">New password:</label>
                            <input type="password" class="form-control col-sm-6" name="newpass" placeholder="Password">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 text-right">Confirm password:</label>
                            <input type="password" class="form-control col-sm-6" name="confpass" placeholder="Password">
                        </div>
                        
                    </div> 
                    <div class="card-footer text-right">
                        <a href=""></a>
                        <button type="submit" class="btn btn-primary">Change password</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<?php endforeach;?>