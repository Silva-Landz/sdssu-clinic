<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container pb-5">
    <div class="row justify-content-center vertical-offset pb-5">		
		<div class="col-lg-4 col-md-5 col-sm-10 col-xs-12">
			<?php echo validation_errors('<p class="alert alert-danger"><span class="fa fa-exclamation-triangle"></span> ', '</p>'); ?>
			<div class="card border-secondary">
				<div class="card-header"><b><span class="fa fa-lock"></span> Login</b></div>
				<div class="card-body">
					<form action="<?php echo base_url('admin/login'); ?>" method="POST">
						<div class="form-group">
							<input type="text" class="form-control" name="username" placeholder="Enter your username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password" placeholder="Enter your password">
						</div>
						<div class="form-group">
							<select name="usertype" class="form-control">
								<option value="administrator">Administrator</option>
								<option value="doctor">Doctor</option>
								<option value="dentist">Dentist</option>
							</select>
						</div>
						<button type="submit" class="form-control btn btn-primary">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>