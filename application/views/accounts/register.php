<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row justify-content-center mt-5">
		<div class="col-md-6">
            <?php 
                echo $this->session->flashdata('error');
                echo $this->session->flashdata('message');
                echo validation_errors('<p class="alert alert-danger"><span class="fa fa-exclamation-triangle"></span> ', '</p>'); 
            ?>
			<div class="card border-secondary">
				<div class="card-header"><b>Register users</b></div>
				<div class="card-body">
					<form action="<?php echo base_url('admin/registration'); ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="firstname">Firstname</label>
                                <input type="text" class="form-control" name="firstname" value="<?php echo set_value('firstname'); ?>" placeholder="Juan dela">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="lastname">Lastname</label>
                                <input type="text" class="form-control" name="lastname" value="<?php echo set_value('lastname'); ?>" placeholder="Cruz">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" name="username" value="<?php echo set_value('username'); ?>" placeholder="juandelacruz">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>	
                            <div class="form-group col-md-6">
                                <label for="confirmpass">Confirm Password</label>
                                <input type="password" class="form-control" name="confirmpass" placeholder="Password">
                            </div>	
						</div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="usertype">User Type</label>
                                <select name="usertype" class="form-control">
                                    <option value="">Choose . . .</option>
                                    <option value="administrator">Administrator</option>
                                    <option value="doctor">Doctor</option>
                                    <option value="dentist">Dentist</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="usertype">Upload photo (optional)</label>
                                <input type="file" name="image" accept="image/*" class="form-control">
                            </div>
                        </div>
						<button type="submit" class="btn btn-primary pull-right">Register</button>
                        <a href="<?php echo base_url('admin/login'); ?>" class="btn btn-light pull-right mr-2">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>