    
    <?php if($title == 'Forbidden' || $title == 'Login'): ?>
            <div></div>
    <?php else: ?>
        <div class="container mt-5">
            <hr>
            <div class="row justify-content-center text-center">
                <p class="text-muted">Surigao del sur State University Clinic Management System <br> All Rights Reserved. Copyright <?php echo date('Y'); ?></p>
            </div>
        </div>
    <?php endif; ?>
    
    <script>var base_url = "<?php echo base_url(); ?>";</script>
    <script src="<?php echo base_url('assets/js/jquery-3.3.1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap4.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dropzone.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
    <script>
        $(document).ready(function()    {
            $('#patients').DataTable();
            $('#dispense').DataTable();
            $('#dental').DataTable();
        });
    </script>
</body>
</html>