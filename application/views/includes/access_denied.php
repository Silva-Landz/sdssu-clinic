<div class="container">
    <div class="row text-center">
        <div class="col-md-12 mt-5" style="font-family: Helvetica">
            <h1 style="font-size: 15rem"><b>403</b></h1>
            <h1 style="font-size: 4rem">Access Denied</h1>
            <hr>
            <p class="lead" style="font-size: 2rem">You do not have permission to view this directory.</p>

            <a href="<?php echo base_url('admin/login'); ?>" class="btn btn-dark btn-lg mt-5"><span class="fa fa-sign-in"></span> Login</a>
        </div>
    </div>
</div>