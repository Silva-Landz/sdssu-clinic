<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css'); ?>">
 
</head>

<?php if($this->uri->segment(2) == 'login'): ?>
        <body id="background">
<?php else: ?>
        <body>
<?php endif; ?>

   <?php if($title == 'Dashboard'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Patient'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Inventory'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Records'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Dispense'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Logs'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'About'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php elseif($title == 'Profile'): ?>
        <?php $this->load->view('includes/navbar'); ?>
        <?php $this->load->view('includes/breadcrumb'); ?>
    <?php endif; ?>