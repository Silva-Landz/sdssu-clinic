<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <b>You are now here:&nbsp</b>
        <?php if($title == 'Dashboard'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Dasboard</li>
        <?php elseif($title == 'Patient'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Patient</li>
        <?php elseif($title == 'Inventory'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Inventory</li>
        <?php elseif($title == 'Records'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Records</li>
        <?php elseif($title == 'Dispense'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Dispense</li>
        <?php elseif($title == 'Logs'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Logs</li>
        <?php elseif($title == 'About'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">About</li>
        <?php elseif($title == 'Profile'): ?>
            <li class="breadcrumb-item"> Home</li>
            <li class="breadcrumb-item active">Profile</li>
        <?php endif; ?>
    </ol>
</nav>