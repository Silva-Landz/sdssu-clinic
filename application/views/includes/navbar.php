<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #3498db">
    <a class="navbar-brand" href="#">SCMS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
        
    <?php 
        
        $users = $this->session->userdata('users');
        // Start of the loop 
        foreach($users as $user): 
            
    ?>
        <?php if($user->user_type == 'administrator'): ?>

        <ul class="navbar-nav">
            <li class="nav-item">
                <?php if($title == 'Dashboard'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-tachometer"></i> Dashboard</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Patient'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-wheelchair"></i> Patient <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/patient'); ?>"><i class="fa fa-wheelchair"></i> Patient</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Inventory'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-archive"></i> Inventory <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/inventory'); ?>"><i class="fa fa-archive"></i> Inventory</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Records'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-files-o"></i> Records <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/records'); ?>"><i class="fa fa-files-o"></i> Records</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Dispense'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-medkit"></i> Dispense <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/dispense'); ?>"><i class="fa fa-medkit"></i> Dispense</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Logs'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-history"></i> History Logs <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/logs'); ?>"><i class="fa fa-history"></i> History Logs</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'About'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-info-circle"></i> About <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/about'); ?>"><i class="fa fa-info-circle"></i> About</span></a>
                <?php endif; ?>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active mr-3" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <!-- Checking the usertype -->
                    <?php 
                        if($user->user_type)  {
                            echo ucfirst($user->firstname) ." ". ucfirst($user->lastname);
                        } 
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php echo base_url('admin/profile/'). $user->id; ?>"><span class="fa fa-user"></span> Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>"><span class="fa fa-sign-out"></span> Logout</a>
                </div>
            </li>
        </ul>
        <?php endif; ?>

        <!-- DOCTOR -->
        <?php if($user->user_type == 'doctor'): ?>
        
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php if($title == 'Dashboard'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-tachometer"></i> Dashboard</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Patient'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-wheelchair"></i> Patient <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/patient'); ?>"><i class="fa fa-wheelchair"></i> Patient</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Records'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-files-o"></i> Records <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/records'); ?>"><i class="fa fa-files-o"></i> Records</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'About'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-info-circle"></i> About <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/about'); ?>"><i class="fa fa-info-circle"></i> About</span></a>
                <?php endif; ?>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active mr-3" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <!-- Checking the usertype -->
                    <?php 
                        if($user->user_type)  {
                            echo ucfirst($user->firstname) ." ". ucfirst($user->lastname);
                        } 
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php echo base_url('admin/profile/'). $user->id; ?>"><span class="fa fa-user"></span> Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>"><span class="fa fa-sign-out"></span> Logout</a>
                </div>
            </li>
        </ul>
        <?php endif; ?>

        <!-- DENTIST -->
        <?php if($user->user_type == 'dentist'): ?>
        
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php if($title == 'Dashboard'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-tachometer"></i> Dashboard</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Patient'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-wheelchair"></i> Patient <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/patient'); ?>"><i class="fa fa-wheelchair"></i> Patient</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'Records'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-files-o"></i> Records <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/records'); ?>"><i class="fa fa-files-o"></i> Records</span></a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if($title == 'About'): ?>
                    <a class="nav-link active" href="#"><i class="fa fa-info-circle"></i> About <span class="sr-only">(current)</span></a>
                <?php else: ?>
                    <a class="nav-link" href="<?php echo base_url('admin/about'); ?>"><i class="fa fa-info-circle"></i> About</span></a>
                <?php endif; ?>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active mr-3" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <!-- Checking the usertype -->
                    <?php 
                        if($user->user_type)  {
                            echo ucfirst($user->firstname) ." ". ucfirst($user->lastname);
                        } 
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php echo base_url('admin/profile/'). $user->id; ?>"><span class="fa fa-user"></span> Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>"><span class="fa fa-sign-out"></span> Logout</a>
                </div>
            </li>
        </ul>
        <?php endif; ?>

    <!-- End of the loop -->
    <?php endforeach; ?> 

    </div>
</nav>