-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2018 at 03:44 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scmsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `scms_course`
--

CREATE TABLE `scms_course` (
  `course_id` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `course_description` varchar(255) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_dental`
--

CREATE TABLE `scms_dental` (
  `dr_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `diagnosis_desc` varchar(255) NOT NULL,
  `findings` varchar(255) NOT NULL,
  `presc_med` int(11) NOT NULL,
  `image_dental` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_department`
--

CREATE TABLE `scms_department` (
  `dept_id` int(11) NOT NULL,
  `dept_code` varchar(50) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_dispense`
--

CREATE TABLE `scms_dispense` (
  `disp_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `illness` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_inventory`
--

CREATE TABLE `scms_inventory` (
  `inv_id` int(11) NOT NULL,
  `inv_name` varchar(255) NOT NULL,
  `inv_category` enum('medicine','ppe','accessory') NOT NULL,
  `inv_stocks` int(11) NOT NULL,
  `inv_price` double NOT NULL,
  `inv_expiration` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_log`
--

CREATE TABLE `scms_log` (
  `log_id` int(11) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_medical`
--

CREATE TABLE `scms_medical` (
  `mr_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `findings` varchar(255) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_patient`
--

CREATE TABLE `scms_patient` (
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  `year` enum('1','2','3','4','5','6') NOT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `bloodtype` enum('A+','A-','B+','B-','O+','O-','AB+','AB-') NOT NULL,
  `tin_no` varchar(20) NOT NULL,
  `philhealth_no` varchar(20) NOT NULL,
  `sLname` varchar(255) NOT NULL,
  `sFname` varchar(255) NOT NULL,
  `sMname` varchar(255) NOT NULL,
  `sOccuptation` varchar(255) NOT NULL,
  `sContact_no` int(11) NOT NULL,
  `fLname` varchar(255) NOT NULL,
  `fFname` varchar(255) NOT NULL,
  `fMname` varchar(255) NOT NULL,
  `sOccupation` varchar(255) NOT NULL,
  `sContact` int(11) NOT NULL,
  `mLname` varchar(255) NOT NULL,
  `mFname` varchar(255) NOT NULL,
  `mMname` varchar(255) NOT NULL,
  `mOccupation` varchar(255) NOT NULL,
  `mContact` int(11) NOT NULL,
  `patient_type` enum('student','personnel') NOT NULL,
  `reg_date` datetime NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scms_users`
--

CREATE TABLE `scms_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `user_type` enum('administrator','doctor','dentist') NOT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scms_users`
--

INSERT INTO `scms_users` (`id`, `username`, `password`, `lastname`, `firstname`, `user_type`, `photo`) VALUES
(1, 'rebueralandz', '1ab18e0f6ccd3c87c735c262f2f1a2cd0aab3794a809c3137fadc604753b99bdd6c03df37c20cbed23503e9c93d965b9f021e0fb991a69cf9e833ca0909142d0', 'Rebuera', 'Landrex', 'administrator', NULL),
(2, 'cuarterojayann', '64f5801ec2c000d8ef453a5ee7075265f60055ba08eb524189247a4f86d9ac619138aca4a070d81a1faf09e81e0c7f3f846bd1b86a2367f8e443a87d2d8dac94', 'Cuartero', 'Jay Ann', 'dentist', NULL),
(3, 'kenngarcia', '8275c69dbf3bdaedb3ab3d16141ad5b068b930b80eceb50471cb6738a84a4e594ab77dc8be9f2c90cae037e8819633e0c679e6f229e1c7a2d6f776f930c8c4cd', 'Garcia', 'Kenn', 'doctor', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scms_course`
--
ALTER TABLE `scms_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `scms_dental`
--
ALTER TABLE `scms_dental`
  ADD PRIMARY KEY (`dr_id`);

--
-- Indexes for table `scms_inventory`
--
ALTER TABLE `scms_inventory`
  ADD PRIMARY KEY (`inv_id`);

--
-- Indexes for table `scms_log`
--
ALTER TABLE `scms_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `scms_medical`
--
ALTER TABLE `scms_medical`
  ADD PRIMARY KEY (`mr_id`);

--
-- Indexes for table `scms_patient`
--
ALTER TABLE `scms_patient`
  ADD PRIMARY KEY (`pid`),
  ADD UNIQUE KEY `spid` (`spid`);

--
-- Indexes for table `scms_users`
--
ALTER TABLE `scms_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scms_course`
--
ALTER TABLE `scms_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_dental`
--
ALTER TABLE `scms_dental`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_inventory`
--
ALTER TABLE `scms_inventory`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_log`
--
ALTER TABLE `scms_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_medical`
--
ALTER TABLE `scms_medical`
  MODIFY `mr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_patient`
--
ALTER TABLE `scms_patient`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scms_users`
--
ALTER TABLE `scms_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
